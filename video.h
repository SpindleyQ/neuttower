/*** V I D E O ***/
#define setMode(hexval) asm { mov ax, hexval; int 10h }

#define setVGAMode() setMode(0013h)
#define setEGAMode() setMode(000Dh)
#define setTextMode() setMode(0003h)

#define REG_AC   0x03c0
#define REG_TS   0x03c4
#define REG_GDC  0x03ce
#define REG_CRTC 0x03d4

#define PLANE_B 0x00
#define PLANE_G 0x01
#define PLANE_R 0x02
#define PLANE_I 0x03
#define setPlane(p) outport(REG_TS, 2 | (0x100 << p))
#define setPlaneColor(c) outport(REG_TS, 2 | (c << 8))
#define setAllPlanes() setPlaneColor(0x0f)

#define setWriteMode(m) outport(REG_GDC, 0x05 | (m << 8))
#define setBitMask(m) outport(REG_GDC, 0x08 | (m << 8))

#define setResetEnabled(m) outport(REG_GDC, 0x01 | (m << 8))
#define setResetMask(m) outport(REG_GDC, m << 8)

#define VID ((volatile char far *)MK_FP(0xa000, 0))
#define WVID ((volatile int far *)MK_FP(0xa000, 0))

#define flipPage(p) outport(REG_CRTC, 0x0c | (p << 8))

#define setLogicalWidth(w) outport(REG_CRTC, 0x13 | (w << 8))

void vid_cleanup();
void setSplitScreen(unsigned int y);
void unsetSplitScreen();
void setDisplayOffset(unsigned int offset);
void setHorizontalPan(int offset);

