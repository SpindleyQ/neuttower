#include "adlib.h"

int adlib_present;
static void adlib_wait(int delay) {
  int i;
  for (i = 0; i < delay; i ++) adlib_read();
}

void adlib_write(int reg, int val) {
  int i;
  outp(0x388, reg);
  adlib_wait(6);
  outp(0x389, val);
  adlib_wait(35);
}

void adlib_reset() {
  int i;
  for (i = 0; i < 0xff; i ++) {
    adlib_write(i, 0);
  }
}

void adlib_detect() {
  int status1, status2;
  adlib_write(4, 0x60);
  adlib_write(4, 0x80);
  status1 = adlib_read();
  adlib_write(2, 0xff);
  adlib_write(4, 0x21);
  adlib_wait(160);
  status2 = adlib_read();
  adlib_present = (status1 & 0xe0) == 0 && (status2 & 0xe0) == 0xc0;
}

void adlib_init() {
  adlib_detect();
  if (adlib_present) {
    adlib_reset();
    atexit(adlib_reset);
  }
}
