#include <dos.h>
#include "dtext.h"

int dtext_x = 0;
int dtext_y = 0;
int dtext_left = 0;
int dtext_attr = 0x1f;

void dtext_emit(char c) {
  if (c == '\n') {
    dtext_cr();
    return;
  }
  if (c == '\r') return;
  WDTEXT[dtext_x + (dtext_y * 80)] = (c) | (dtext_attr << 8);
  dtext_x ++;
  if (dtext_x >= 80) {
    dtext_cr();
  }

}

void dtext_emitattr(char attr) {
  DTEXT[(dtext_x << 1) + (dtext_y * 160) + 1] = attr;
  dtext_x ++;
  if (dtext_x >= 80) {
    dtext_cr();
  }
}
