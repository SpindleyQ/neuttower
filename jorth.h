
#include <stdio.h>

#define MEM_SIZE 40960
#define STACK_SIZE 64
#define RSTACK_SIZE 64

void f_init(char *exe);

void f_cdef();
void f_immediate();

void f_loadfile(char *filename);
void f_runstring(char *s);

void f_quiet();
void f_loud();
void f_interpreter();

union cell_union;
typedef union cell_union cell;

union cell_union {
  int i;
  unsigned int u;
  cell *p;
  char *s;
  void (*f)();
  FILE *fp;
};

extern char mem[MEM_SIZE];
extern cell *HERE;
extern cell *LATEST;
extern cell IP;
extern cell W;
extern cell *rstack;
extern cell *stack;

#define F_NAMELEN_MASK 0x7f
#define F_IMMEDIATE 0x80

#define CELL_OFFSET(cp, b) ((cell*)(((char *)(cp)) + b))
#define TOP() (*(stack - 1))
#define ST1() (*(stack - 2))
#define ST2() (*(stack - 3))
void DROP(int n);
void PUSHC(cell c);
void PUSHI(int i);
void PUSHU(unsigned int u);
void PUSHCP(cell *c);
#define PUSHP(p) PUSHCP((cell*)p)
void PUSHS(char *s);
void RPUSH(cell c);
#define RPOP() (--rstack)
#define RTOP() (*(rstack - 1))

void f_key();
void f_word();
void f_emit();
void f_puts();
void f_dot();
void f_cr();
void f_comma();
void f_bcomma();
void f_create(); // name --
void f_cdef(); // func name --
void f_doconst();
void f_compileword();

cell f_lookupcp(char *name);
void f_execcp(cell cp);

#define CDEF(name, def) PUSHP(def); PUSHS(name); f_cdef()
#define ICONST(name, v) CDEF(name, f_doconst); PUSHI(v); f_comma()
#define PCONST(name, p) CDEF(name, f_doconst); PUSHP(p); f_comma()
