#include <dir.h>
#include <stdlib.h>
#include <time.h>
#include "jorth.h"
#include "adlib.h"
#include "kbd.h"
#include "timer.h"
#include "serial.h"
#include "dtext.h"

cell ontick = 0;
void f_adlib_read() {
  PUSHU(adlib_read());
}

void f_adlib_write() {
  adlib_write(TOP().u & 0xff, ST1().u & 0xff);
  DROP(2);
}

volatile int WAKE = 0;
static void timer_callback() {
  if (adlib_read() & 0x20) {
    WAKE = 1;
  }
}

int DONE = 0;
static void f_quit() {
  DONE = 1;
}

void f_keyWasPressed() {
  int k = TOP().i;
  TOP().i = keyWasPressed(k);
  consumeKey(k);
}

void f_keydown() {
  TOP().i = keyIsDown(TOP().i);
}

char *gather_input() {
  static char buf[128];
  static int ibuf = 0;

  if (bioskey(1)) {
    int key = bioskey(0);
    char ch = key & 0xff;
    if (ch == 0x08) {
      if (ibuf > 0) {
	printf("%c %c", ch, ch);
	ibuf --;
      }
    } else {
      buf[ibuf] = ch;
      ibuf ++;
      if (ch == 0x0d) {
	printf("\n");
	buf[ibuf] = 0;
	ibuf = 0;
	return buf;
      } else {
	printf("%c", ch);
      }
    }
  }
  return NULL;
}

void f_seremit() {
  ser_write_byte(TOP().i);
  if (TOP().i == '\n') {
    ser_write_byte('\r');
  }
  DROP(1);
}

void f_random() {
  TOP().i = random(TOP().i);
}

void f_dtextemit() {
  dtext_emit(TOP().i);
  DROP(1);
}
void f_dtextemitattr() {
  dtext_emitattr(dtext_attr);
}

void f_poll_ser() {
  static char line[128] = { 0 };

  if (ser_getline(line)) {
    PUSHS(line);
    f_runstring("DBG send");
    f_taskloop();
    line[0] = '\0';
  }
}

void do_repl(char *exe) {
  adlib_init();

  timer_init(TIMER_18HZ);
  f_init(exe);

  ser_init(SER_COM2, BAUD_19200, SER_8N1);
  CDEF("seremit", f_seremit);
  CDEF("_quit", f_quit);
  CDEF("adlib!", f_adlib_write);
  CDEF("adlib@", f_adlib_read);
  CDEF("key-start", kbd_init);
  CDEF("key-end", kbd_cleanup);
  CDEF("key-debounce", kbd_debounce);
  CDEF("key-pressed", f_keyWasPressed);
  CDEF("key-down", f_keydown);
  CDEF("rnd", f_random);
  PCONST("textx", &dtext_x);
  PCONST("texty", &dtext_y);
  PCONST("textleft", &dtext_left);
  PCONST("textattr", &dtext_attr);
  CDEF("emit-direct", f_dtextemit);
  CDEF("attremit", f_dtextemitattr);

  f_loadfile("jopl.jor");
  ontick = f_lookupcp("ontick");
  timer_setcallback(timer_callback);

  f_taskloop();

  while (!DONE) {
    char *buf = gather_input();
    if (buf) {
      PUSHS(buf);
      f_runstring("REPL send");
    }
    if (WAKE) {
      WAKE = 0;
      if (ontick.p != NULL) {
	f_execcp(ontick);
      }
    }
    f_poll_ser();
    f_taskloop();
  }
}

#define RIGHT  0x01
#define LEFT   0x02
#define CTRL   0x04
#define ALT    0x08

void keything() {
  int key, modifiers, done;
  done = 0;
  while (!done) {
    /* function 1 returns 0 until a key is pressed */
    while (bioskey(1) == 0);

    /* function 0 returns the key that is waiting */
    key = bioskey(0);

    /* use function 2 to determine if shift keys were used */
    modifiers = bioskey(2);
    if (modifiers)
    {
      printf("[%#02x", modifiers);
      if (modifiers & RIGHT) printf(" RIGHT");
      if (modifiers & LEFT)  printf(" LEFT");
      if (modifiers & CTRL)  printf(" CTRL");
      if (modifiers & ALT)   printf(" ALT");
      printf("]");
    }
    /* print out the character read */
    printf("'%c' %#02x\n", key & 0xff, key);
    if ((key & 0xff) == 'q') done = 1;

  }
}
int main(int argc, char *argv[]) {
//  keything();
  randomize();
  do_repl(argv[0]);
  return 0;
}