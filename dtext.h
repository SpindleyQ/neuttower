/* D I R E C T   T E X T M O D E */

extern int dtext_x;
extern int dtext_y;
extern int dtext_left;
extern int dtext_attr;

#define DTEXT ((volatile char far *)MK_FP(0xb800, 0))
#define WDTEXT ((volatile int far *)MK_FP(0xb800, 0))

#define dtext_cr() { dtext_x = dtext_left; dtext_y ++; }
void dtext_emit(char c);
void dtext_emitattr(char attr);
