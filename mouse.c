#include <dos.h>
#include "mouse.h";

/*** M O U S E ***/
Mouse_t MOUSE;

void far mouse_callback() {
  asm {
    mov ax, DGROUP
    mov ds, ax
    shr cx, 1
    mov MOUSE.x, cx
    mov MOUSE.y, dx
    mov MOUSE.buttons, bx
  }
}

void mouse_cleanup() {
  //uninstall handler
  asm {
    mov ax, 0ch
    mov dx, 0
    mov es, dx
    mov cx, 0
    int 33h

    xor ax, ax
    int 33h
  }
}

void mouse_init() {
  unsigned seg_mouse_callback = FP_SEG(mouse_callback);
  unsigned off_mouse_callback = FP_OFF(mouse_callback);
  unsigned int result;
  asm {
    xor ax, ax
    int 33h
    mov result, ax
  }
  if (result == 0) {
    printf("Mouse driver not installed\n");
    exit(1);
  }
  atexit(mouse_cleanup);

  asm {
    mov ax, seg_mouse_callback
    mov es, ax
    mov dx, off_mouse_callback
    mov ax, 0ch
    mov cx, 1fh
    int 33h
  }
}
