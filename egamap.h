#include "tiles.h"

#define NUM_TILES 64
#define NUM_PORTRAITS 16

#define SIZE_FOOTER (PAGE_STRIDE * 48)
#define SIZE_PAGE (PAGE_STRIDE * (PAGE_TILES_H << 4))
#define SIZE_TILES (NUM_TILES << 5)
#define SIZE_PORTRAITS (NUM_PORTRAITS << 7)

#define OFF_FOOTER 0
#define OFF_PAGE1 (((OFF_FOOTER + SIZE_FOOTER) + 0x100) & 0xff00)
#define OFF_PAGE2 (((OFF_PAGE1 + SIZE_PAGE) + 0x100) & 0xff00)
#define OFF_TILES (OFF_PAGE2 + SIZE_PAGE)
#define OFF_PORTRAITS (OFF_TILES + SIZE_TILES)
